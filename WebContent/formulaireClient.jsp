<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Form</title>
<c:import url="WEB-INF/inc/link.jsp" />

</head>
<body class="bg-info">
	<c:import url="WEB-INF/inc/header.jsp" />
	<div class="container m-4 bg-light border rounded m-auto">
	<c:if test="${show eq true}">
		<div class="alert alert-danger m-2" role="alert">
			<c:out value="${ result }" />
		</div>
	</c:if>
	
		<form action=<c:url value="CreationClient"/> method="POST">
			<c:import url="WEB-INF/inc/inc_form_client.jsp"/>

			<button type="submit" class="btn btn-primary m-3">Submit</button>
		</form>
	</div>
	<c:import url="WEB-INF/inc/script.jsp" />
</body>
</html>
