
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Adresse</title>
<c:import url="WEB-INF/inc/link.jsp" />
</head>
<body class="bg-info">
	<c:import url="WEB-INF/inc/header.jsp" />
	<div class="container m-4 bg-light border rounded m-auto">
	<c:if test="${show eq true}">
		<div class="alert alert-danger m-2" role="alert">
			<c:out value="${ result }" />
		</div>
	</c:if>
		<form method="POST" action=<c:url value="CreationAdresse"/>>
			<c:import url="WEB-INF/inc/inc_form_client.jsp" />
			<div class="form-group m-3">
				<label for="rue">rue</label> <input type="text" class="form-control"
					id="rue" name="rue" placeholder="Entrer votre rue"
					value="${adresse.rue}"> <small class="form-text text-muted">${formA.erreurs["rue"]}</small>
			</div>
			<div class="form-group m-3">
				<label for="codePostal">code postal</label> <input type="text"
					class="form-control" id="codePostal" name="codePostal"
					placeholder="Entrer votre code postal"
					value="${adresse.codePostal}"> <small
					class="form-text text-muted">${formA.erreurs["cp"]}</small>
			</div>
			<div class="form-group m-3">
				<label for="ville">ville</label> <input type="text"
					class="form-control" id="ville" name="ville"
					placeholder="Entrer votre ville" value="${adresse.ville}">
				<small class="form-text text-muted">${formA.erreurs["ville"]}</small>
			</div>

			<button type="submit" class="btn btn-primary m-3">Submit</button>
		</form>
	</div>
	<c:import url="WEB-INF/inc/script.jsp" />
</body>
</html>
