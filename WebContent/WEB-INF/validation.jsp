<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Success Client</title>
<c:import url="inc/link.jsp" />
</head>
<body class="bg-info">
	<c:import url="inc/header.jsp" />
	<div class="container-fluid">
	<div class="jumbotron  m-4">
		<h1 class="display-4">Cr�ation client</h1>
		<div class="alert alert-info m-2" role="alert">
			<c:out value="${ result }" />
		</div>
		<c:set scope="request" var="c" value="${client}" />
		<p class="lead">
			Bienvenue
			<c:out value="${c.nom} ${c.prenom}" />
		</p>
	</div>
	</div>
	<c:import url="inc/script.jsp" />
</body>
</html>
