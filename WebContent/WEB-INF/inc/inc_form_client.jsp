
<div class="form-group m-3">
	<label for="nom">Nom</label> <input type="text" class="form-control"
		id="nom" aria-describedby="nom" name="nom"
		placeholder="Entrer votre nom" value="${client.nom}"> <small
		id="emailHelp" class="form-text text-muted">${form.erreurs['nom']}</small>
</div>
<div class="form-group m-3">
	<label for="prenom">Pr�nom</label> <input type="text"
		class="form-control" id="prenom" name="prenom"
		placeholder="Entrer votre prenom" value="${client.prenom}"> <small
		id="emailHelp" class="form-text text-muted">${form.erreurs["prenom"]}</small>
</div>
<div class="form-group m-3">
	<label for="telephone">T�lephone</label> <input type="text"
		class="form-control" id="telephone" name="telephone"
		placeholder="Entrer votre t�lephone" value="${client.telephone}">
	<small id="emailHelp" class="form-text text-muted">${form.erreurs["telephone"]}</small>
</div>
<div class="form-group m-3">
	<label for="email">Email</label> <input type="email"
		class="form-control" id="email" name="email"
		placeholder="Entrer votre email" value="${client.email}">
	<small id="emailHelp" class="form-text text-muted">${form.erreurs["email"]}</small>
</div>