<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<c:import url="inc/link.jsp" />

</head>
<body class="bg-info">
	<c:import url="inc/header.jsp"/>
	<div class="container-fluid">
		<div class="jumbotron jumbotron">
			<h1 class="display-4">Commande</h1>
			<div class="alert alert-primary" role="alert">
				<c:out value="${ result }" />
			</div>

			<c:set scope="request" var="a" value="${adresse}" />
			<p class="lead">
				Bienvenue
				<c:out value="${a.client.nom} ${a.client.prenom} " />
			</p>
			<h2>Votre adresse:</h2>
			<ul>
				<li><c:out value="${a.rue}" /></li>
				<li><c:out value="${a.codePostal}" /></li>
				<li>${a.ville}</li>
			</ul>
		</div>
	</div>
	<c:import url="inc/script.jsp" />
</body>
</html>
