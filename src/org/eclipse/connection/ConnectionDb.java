package org.eclipse.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDb {
	private static String url = "jdbc:mysql://localhost:8890/Tp_form?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
	private static String utilisateur = "root";
	private static String motDePasse = "root";
	private static Connection connexion = null;
	
	private ConnectionDb() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connexion = DriverManager.getConnection(url, utilisateur, motDePasse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Connection getConnection() {
		if (connexion == null) {
			new ConnectionDb();
		}
		return connexion;
	}

	public static void stop() {
		if (connexion != null) {
			try {
				connexion.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
