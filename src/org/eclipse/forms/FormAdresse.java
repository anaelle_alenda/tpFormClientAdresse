package org.eclipse.forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.beans.Adresse;


public class FormAdresse {
	private static final String CHAMP_RUE = "rue";
	private static final String CHAMP_CODEPOSTAL = "codePostal";
	private static final String CHAMP_VILLE = "ville";

	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();

	public String getResultat() {
		return resultat;
	}

	public Map<String, String> getErreurs() {
		return erreurs;
	}

	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	private static String getValeurChamp(HttpServletRequest request, String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur;
		}
	}

	public Adresse createAdresse(HttpServletRequest request) {
		String rue = getValeurChamp(request, CHAMP_RUE);
		String codePostal = getValeurChamp(request, CHAMP_CODEPOSTAL);
		String ville = getValeurChamp(request, CHAMP_VILLE);
		Adresse adresse = new Adresse();
		try {
			checkAtLeast2Char(rue);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			setErreur(CHAMP_RUE, e.getMessage());
		}
		adresse.setRue(rue);
		try {
			checkCpNumber(codePostal);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			setErreur(CHAMP_CODEPOSTAL, e.getMessage());
		}
		adresse.setCodePostal(codePostal);
		try {
			checkAtLeast2Char(ville);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			setErreur(CHAMP_VILLE, e.getMessage());
		}
		adresse.setVille(ville);

		if (erreurs.isEmpty()) {
			resultat = "Création Adresse: succès";
		} else {
			resultat = "Création Adresse: échec";
		}
		return adresse;
	}

	private void checkAtLeast2Char(String champ) throws Exception {
		if (champ != null) {
			if (champ.length() <= 2) {
				throw new Exception("Ce champ doit contenir au minimun deux caractères");
			}
			for (int i = 0; i < champ.length(); i++) {
				char c = champ.charAt(i);
				if (!(c >= 'a' && c <= 'z') && !(c >= 'A' && c <= 'Z')) {
					throw new Exception("Ce champ doit contenir seulement des caractères");
				}
			}

		} else {
			throw new Exception("Ce champ doit être rempli");
		}
	}

	private void checkCpNumber(String champ) throws Exception {
		if (champ != null) {
			if (champ.length() >= 4) {
				try {
					Integer.parseInt(champ);
				} catch (NumberFormatException e) {
					throw new Exception("Ce champ doit contenir des chiffres");
				}
			} else {
				throw new Exception("Ce champ doit contenir au moins 4 chiffres");
			}
		} else {
			throw new Exception("Ce champ doit être rempli");
		}
	}
}
