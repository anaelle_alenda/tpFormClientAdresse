package org.eclipse.forms;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.beans.Client;

public class FormClient {
	private static final String CHAMP_NOM = "nom";
	private static final String CHAMP_PRENOM = "prenom";
	private static final String CHAMP_TELEPHONE = "telephone";
	private static final String CHAMP_MAIL ="email";
	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();

	public String getResultat() {
		return resultat;
	}

	public Map<String, String> getErreurs() {
		return erreurs;
	}

	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	private static String getValeurChamp(HttpServletRequest request, String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur;
		}
	}

	public Client createClient(HttpServletRequest request) {
		String nom = getValeurChamp(request, CHAMP_NOM);
		String prenom = getValeurChamp(request, CHAMP_PRENOM);
		String telephone = getValeurChamp(request, CHAMP_TELEPHONE);
		String email = getValeurChamp(request, CHAMP_MAIL);
		Client client = new Client();
		try {
			checkAtLeast2Char(nom);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			setErreur(CHAMP_NOM, e.getMessage());
		}
		client.setNom(nom);
		try {
			checkAtLeast2Char(prenom);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			setErreur(CHAMP_PRENOM, e.getMessage());
		}
		client.setPrenom(prenom);
		try {
			checkPhoneNumber(telephone);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			setErreur(CHAMP_TELEPHONE, e.getMessage());
		}
		client.setTelephone(telephone);
		try {
			checkEmail(email);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			setErreur(CHAMP_MAIL,e.getMessage());
		}
		client.setEmail(email);

		if (erreurs.isEmpty()) {
			resultat = "Création client : succès";
		} else {
			resultat = "Création client: échec";
		}
		return client;
	}

	private void checkAtLeast2Char(String champ) throws Exception {
		if (champ != null) {
			if (champ.length() <= 2) {
				throw new Exception("Ce champ doit contenir au minimun deux caractères");
			}
			for (int i = 0; i < champ.length(); i++) {
				char c = champ.charAt(i);
				if (!(c >= 'a' && c <= 'z') && !(c >= 'A' && c <= 'Z')) {
					throw new Exception("Ce champ doit contenir seulement des caractères");
				}
			}

		} else {
			throw new Exception("Ce champ doit être rempli");
		}
	}

	private void checkPhoneNumber(String champ) throws Exception {
		if (champ != null) {
			if (champ.length() == 10) {
				try {
					Integer.parseInt(champ);
				} catch (NumberFormatException e) {
					throw new Exception("Ce champ doit contenir des chiffres");
				}
			} else {
				throw new Exception("Ce champ doit contenir 10 chiffres");
			}
		} else {
			throw new Exception("Ce champ doit être rempli");
		}
	}
	private void checkEmail(String champ) throws Exception{
		if(champ != null) {
			String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(champ);
			if(!(matcher.matches())){
				throw new Exception("Ce champ doit contenir un email valide");
			}
		}
		else {
			throw new Exception("Ce champ doit être rempli");
		}
	}
}
