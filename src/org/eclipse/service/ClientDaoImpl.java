package org.eclipse.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.eclipse.beans.Client;
import org.eclipse.connection.ConnectionDb;

public class ClientDaoImpl implements GeneriqueDAO<Client>{
	public Client save(Client client) throws ClassNotFoundException{
		// TODO Auto-generated method stub
		Connection connect = ConnectionDb.getConnection();
		if(connect != null) {
			try {
			String request = "INSERT INTO client(nom,prenom,telephone,email) VALUES (?,?,?,?);";
			PreparedStatement ps = connect.prepareStatement(request,PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1,client.getNom());
			ps.setString(2,client.getPrenom());
			ps.setString(3,client.getTelephone());
			ps.setString(4,client.getEmail());
			int result = ps.executeUpdate();
			ResultSet res = ps.getGeneratedKeys();
			String response = (result == 1) ? "Ajout effectué" : "Erreur lors de l'ajout";
			while (res.next()) {
				client.setId(res.getInt(1));
				return client;
			}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
