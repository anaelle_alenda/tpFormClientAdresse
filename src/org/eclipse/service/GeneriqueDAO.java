package org.eclipse.service;

public interface GeneriqueDAO<T> {
	public T save(T object) throws ClassNotFoundException;
}
