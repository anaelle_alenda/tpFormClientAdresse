package org.eclipse.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.eclipse.beans.Adresse;
import org.eclipse.connection.ConnectionDb;

public class AdresseDaoImpl implements GeneriqueDAO<Adresse> {
	@Override
	public Adresse save(Adresse adresse) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		Connection connect = ConnectionDb.getConnection();
		if(connect != null) {
		try {
			String request = "INSERT INTO adresse(rue,codePostal,ville,idClient) VALUES (?,?,?,?);";
			PreparedStatement ps = connect.prepareStatement(request,PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1,adresse.getRue());
			ps.setString(2,adresse.getCodePostal());
			ps.setString(3,adresse.getVille());
			ps.setInt(4,adresse.getClient().getId());
			int result = ps.executeUpdate();
			ResultSet res = ps.getGeneratedKeys();
			String response = (result == 1) ? "Ajout effectué" : "Erreur lors de l'ajout";
			System.out.println(response);
			while (res.next()) {
				adresse.setId(res.getInt(1));
				return adresse;
			}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}

