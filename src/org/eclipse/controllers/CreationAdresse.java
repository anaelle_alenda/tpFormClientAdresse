package org.eclipse.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.beans.Adresse;
import org.eclipse.beans.Client;
import org.eclipse.forms.FormAdresse;
import org.eclipse.forms.FormClient;
import org.eclipse.service.AdresseDaoImpl;
import org.eclipse.service.ClientDaoImpl;

/**
 * Servlet implementation class creationAdresse
 */
@WebServlet("/CreationAdresse")
public class CreationAdresse extends HttpServlet {
	public static final String ATT_ADRESSE = "adresse";
	public static final String ATT_CLIENT = "client";
	public static final String VUE_SUCCES = "/WEB-INF/validation2.jsp";
	public static final String VUE_FORM = "/formulaireAdresse.jsp";
	public static final String ATT_FORM = "form";
	public static final String ATT_FORMA = "formA";
	public static final String ATT_REST = "result";
	private static final long serialVersionUID = 1L;

	boolean show = false;

	/**
	 * Default constructor.
	 */
	public CreationAdresse() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		FormClient form = new FormClient();
		FormAdresse formA = new FormAdresse();
		Client client = form.createClient(request);
		Adresse adresse = formA.createAdresse(request);

		request.setAttribute(ATT_ADRESSE, adresse);
		request.setAttribute(ATT_CLIENT, client);
		request.setAttribute(ATT_FORM, form);
		request.setAttribute(ATT_FORMA, formA);
		request.setAttribute(ATT_REST, formA.getResultat());
		request.setAttribute("show", show);
		adresse.setClient(client);
		if (formA.getErreurs().isEmpty() && form.getErreurs().isEmpty()) {

			AdresseDaoImpl ad = new AdresseDaoImpl();
			ClientDaoImpl cd = new ClientDaoImpl();
			try {
				cd.save(client);
				ad.save(adresse);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.getServletContext().getRequestDispatcher(VUE_SUCCES).forward(request, response);

		} else {
			show = true;
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		}
	}

}