package org.eclipse.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.beans.Client;
import org.eclipse.forms.FormClient;

import org.eclipse.service.ClientDaoImpl;

/**
 * Servlet implementation class Formulaire
 */
@WebServlet("/CreationClient")
public class CreationClient extends HttpServlet {
	public static final String ATT_CLIENT = "client";
	public static final String VUE_SUCCES = "/WEB-INF/validation.jsp";
	public static final String VUE_FORM = "/formulaireClient.jsp";
	public static final String ATT_FORM = "form";
	public static final String ATT_REST = "result";
	private static final long serialVersionUID = 1L;
	boolean show = false;

	/**
	 * Default constructor.
	 */
	public CreationClient() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		FormClient formC = new FormClient();
		Client client = formC.createClient(request);
		ClientDaoImpl ci = new ClientDaoImpl();
		request.setAttribute(ATT_CLIENT, client);
		request.setAttribute(ATT_FORM, formC);
		request.setAttribute(ATT_REST, formC.getResultat());
		request.setAttribute("show", show);
		
		if (formC.getErreurs().isEmpty()) {
			try {
				ci.save(client);

			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
//				this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
				e.printStackTrace();
			}
			this.getServletContext().getRequestDispatcher(VUE_SUCCES).forward(request, response);
		}else {
			show = true;
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		}

	}

}
